const int pin_pwm = 3;
const int pin_in1 = 6;
const int pin_in2 = 5;
const int pin_stby= 4;

const int pin_measure[2] = {A0, A1};
const int pin_pot = A3;

enum{
  DIR_CW,
  DIR_CCW,
  DIR_NONE
};


void setup() {
  // put your setup code here, to run once:

  pinMode(pin_pwm, OUTPUT);
  pinMode(pin_in1, OUTPUT);
  pinMode(pin_in2, OUTPUT);
  pinMode(pin_stby, OUTPUT);

  pinMode(A2, OUTPUT);
  digitalWrite(A2, LOW);
  

  digitalWrite(pin_stby, HIGH);
  digitalWrite(pin_pwm, HIGH);
  digitalWrite(pin_in1, LOW);
  digitalWrite(pin_in2, LOW);

  Serial.begin(115200);
}


void loop(){

  const int v0 = analogRead(pin_measure[0]);
  const int v1 = analogRead(pin_measure[1]);
  const int gain = analogRead(pin_pot)/4;

  const int sens = (v0>v1) ? -v0 : v1;  // back EMF
  const int sens_thr = 50;

  const int out = (sens*gain)/128;

  if(sens<-sens_thr){
    // CCW
    analogWrite(pin_in2, 0);
    analogWrite(pin_in1, -out);
  }
  else if(sens>sens_thr){
    // CW
    analogWrite(pin_in1, 0);
    analogWrite(pin_in2, out);
  }
  else{
    analogWrite(pin_in1, 0);
    analogWrite(pin_in2, 0);
  }
  delay(10);

  // free roll
  analogWrite(pin_in1, 0);
  analogWrite(pin_in2, 0);
  delay(2);

}
